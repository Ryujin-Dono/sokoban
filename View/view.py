from PyQt5 import QtGui
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtMultimedia import QSound

from View.viewWeb import *


class Plateau(QMainWindow):
    def __init__(self):
        super().__init__()
        #parametre de la fenetre
        self.setWindowTitle("Sokoban")
        self.setWindowIcon(QtGui.QIcon("./View/image/hellicon.png"))
        self.setGeometry(500,50,800,920)

        self.__min=0
        self.__sec=0

        self.timer=QTimer()
        self.timer.timeout.connect(self.updateTimer)
        self.timer.start(1000)

        self.__controller = None
        self.__model = None

        self.__pas = 0

        # label mouvable
        self.__labelC = None
        self.__labelCinv = None
        self.__labelB1 = None 
        self.__labelB2 = None 
        self.__labelB3 = None 
        self.__labelB4 = None 
        self.__labelB5 = None 
        self.__labelB6 = None 
        self.__labelB7 = None 

        self.__labelBS1 = None 
        self.__labelBS2 = None 
        self.__labelBS3 = None 
        self.__labelBS4 = None 
        self.__labelBS5 = None 
        self.__labelBS6 = None 
        self.__labelBS7 = None 
        # appel fonction d'affichage objet
        self.affiche()

        self.__music = QSound("./View/sound/Yhorm_the_Giant.wav")
        self.__music.play()
        
        #affichage
        self.show()

        self.__cheatCode = ['s','a','u','m','o','n','f','u','m','e']
        self.__cheatCodeTmp = []
        self.initUI()
    

    def initUI(self):
        exitAct = QAction(QIcon('Quitter.png'), '&Quitter    Esc', self)
        exitAct.setStatusTip('Quitter')
        exitAct.triggered.connect(qApp.quit)

        restart = QAction(QIcon('Restart.png'), '&Restart    R', self)
        restart.setStatusTip('Restart')
        restart.triggered.connect(self.restartGame)

        infoActH = QAction(QIcon('Haut.png'), '&Haut   ↑', self)
        infoActH.setStatusTip('Haut')
        

        infoActB = QAction(QIcon('Bas.png'), '&Bas    ↓', self)
        infoActB.setStatusTip('Bas')

        infoActD = QAction(QIcon('Gauche.png'), '&Gauche    ←', self)
        infoActD.setStatusTip('Gauche')

        infoActG = QAction(QIcon('Droite.png'), '&Droite    →', self)
        infoActG.setStatusTip('Droite')

        self.statusBar()

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&Menu')
        fileMenu.addAction(restart)
        fileMenu.addAction(exitAct)
        
        fileMenu1 = menubar.addMenu('&Informations')
        fileMenu1.addAction(infoActH)
        fileMenu1.addAction(infoActB)
        fileMenu1.addAction(infoActG)
        fileMenu1.addAction(infoActD)
        self.show()

        self.statusBar().showMessage("Timer: "+str(self.__min)+"m "+str(self.__sec)+"s "+"  | Déplacements : " + str(self.__pas))

    def updateTimer(self):
        if(self.__sec==60):
            self.__min+=1
            self.__sec=0
        self.__sec+=1
        self.statusBar().showMessage("Timer: "+str(self.__min)+"m "+str(self.__sec)+"s "+"  | Déplacements : " + str(self.__pas))
        
    #fonction qui permette d'afficher les image avec i posX et j posY
    def wall(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall.png)")
    
    def wall1(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall1.png)")

    def wall2(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall2.png)")
    
    def wall3(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall3.png)")
    
    def wall4(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall4.png)")
    
    def wall5(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall5.png)")
    
    def wall6(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall6.png)")
    
    def wall7(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall7.png)")
    
    def wall8(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall8.png)")
    
    def wall9(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall9.png)")
    
    def wall10(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall10.png)")
    
    def wall11(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall11.png)")
    
    def wall12(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall12.png)")
    
    def wall13(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall13.png)")
    
    def wall14(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall14.png)")
    
    def wall15(self,i,j):
        self.__labelW = QLabel(self)
        self.__labelW.setGeometry(i,j,100,100)
        self.__labelW.setStyleSheet("background-image:url(./View/image/wall15.png)")
    
    
    def floor(self,i,j):
        self.__labelF = QLabel(self)
        self.__labelF.setGeometry(i,j,100,100)
        self.__labelF.setStyleSheet("background-image:url(./View/image/floor.png)")
    
    def water(self,i,j,k):
        if(k==1):
            self.__labelW = QLabel(self)
            self.__labelW.setGeometry(i,j,100,100)
            self.__labelW.setStyleSheet("background-image:url(./View/image/water.png)")
        elif(k==2):
            self.__labelW = QLabel(self)
            self.__labelW.setGeometry(i,j,100,100)
            self.__labelW.setStyleSheet("background-image:url(./View/image/water2.png)")
        elif(k==3):
            self.__labelW = QLabel(self)
            self.__labelW.setGeometry(i,j,100,100)
            self.__labelW.setStyleSheet("background-image:url(./View/image/water3.png)")
        elif(k==4):
            self.__labelW = QLabel(self)
            self.__labelW.setGeometry(i,j,100,100)
            self.__labelW.setStyleSheet("background-image:url(./View/image/water4.png)")
        
        

    def character(self,i,j):
        self.__labelC = QLabel(self)
        self.__labelC.setGeometry(i,j,100,100)
        self.__labelC.setStyleSheet("background-image:url(./View/image/perso.png)")
    
    def characterInv(self,i,j):
        self.__labelCinv = QLabel(self)
        self.__labelCinv.setGeometry(i,j,100,100)
        self.__labelCinv.setStyleSheet("background-image:url(./View/image/persoInv.png)")
    

    def box1(self,i,j):
        self.__labelB1 = QLabel(self)
        self.__labelB1.setGeometry(i,j,100,100)
        self.__labelB1.setStyleSheet("background-image:url(./View/image/box.png)")
    
    def box2(self,i,j):
        self.__labelB2 = QLabel(self)
        self.__labelB2.setGeometry(i,j,100,100)
        self.__labelB2.setStyleSheet("background-image:url(./View/image/box.png)")
    
    def box3(self,i,j):
        self.__labelB3 = QLabel(self)
        self.__labelB3.setGeometry(i,j,100,100)
        self.__labelB3.setStyleSheet("background-image:url(./View/image/box.png)")
    
    def box4(self,i,j):
        self.__labelB4 = QLabel(self)
        self.__labelB4.setGeometry(i,j,100,100)
        self.__labelB4.setStyleSheet("background-image:url(./View/image/box.png)")

    def box5(self,i,j):
        self.__labelB5 = QLabel(self)
        self.__labelB5.setGeometry(i,j,100,100)
        self.__labelB5.setStyleSheet("background-image:url(./View/image/box.png)")
    
    def box6(self,i,j):
        self.__labelB6 = QLabel(self)
        self.__labelB6.setGeometry(i,j,100,100)
        self.__labelB6.setStyleSheet("background-image:url(./View/image/box.png)")
    
    def box7(self,i,j):
        self.__labelB7 = QLabel(self)
        self.__labelB7.setGeometry(i,j,100,100)
        self.__labelB7.setStyleSheet("background-image:url(./View/image/box.png)")
    
    #boite sur star
    def boxS1(self,i,j):
        self.__labelBS1 = QLabel(self)
        self.__labelBS1.setGeometry(i,j,100,100)
        self.__labelBS1.setStyleSheet("background-image:url(./View/image/boxOnStar.png)")
    
    def boxS2(self,i,j):
        self.__labelBS2 = QLabel(self)
        self.__labelBS2.setGeometry(i,j,100,100)
        self.__labelBS2.setStyleSheet("background-image:url(./View/image/boxOnStar.png)")
    
    def boxS3(self,i,j):
        self.__labelBS3 = QLabel(self)
        self.__labelBS3.setGeometry(i,j,100,100)
        self.__labelBS3.setStyleSheet("background-image:url(./View/image/boxOnStar.png)")
    
    def boxS4(self,i,j):
        self.__labelBS4 = QLabel(self)
        self.__labelBS4.setGeometry(i,j,100,100)
        self.__labelBS4.setStyleSheet("background-image:url(./View/image/boxOnStar.png)")

    def boxS5(self,i,j):
        self.__labelBS5 = QLabel(self)
        self.__labelBS5.setGeometry(i,j,100,100)
        self.__labelBS5.setStyleSheet("background-image:url(./View/image/boxOnStar.png)")
    
    def boxS6(self,i,j):
        self.__labelBS6 = QLabel(self)
        self.__labelBS6.setGeometry(i,j,100,100)
        self.__labelBS6.setStyleSheet("background-image:url(./View/image/boxOnStar.png)")
    
    def boxS7(self,i,j):
        self.__labelBS7 = QLabel(self)
        self.__labelBS7.setGeometry(i,j,100,100)
        self.__labelBS7.setStyleSheet("background-image:url(./View/image/boxOnStar.png)")
    

    def star(self,i,j):
        self.__labelS = QLabel(self)
        self.__labelS.setGeometry(i,j,100,100)
        self.__labelS.setStyleSheet("background-image:url(./View/image/star.png)")

    

    def affiche(self):
        #placement des murs
        for i in range(300,700,100):
            self.wall(i,0)
        self.wall(100,100)

        for i in range(400,800,100):
            self.wall6(0,i)
        self.wall6(0,200)

        for i in range(100,500,100):
            self.wall5(600,i)
        self.wall5(700,600)
        self.wall5(700,700)

        for i in range(100,700,100):
            self.wall7(i,800)
        
        self.wall1(0,100)
        self.wall1(200,0)
        self.wall3(0,800)
        self.wall4(700,800)
        self.wall2(700,500)
        self.wall8(200,100)
        self.wall9(600,500)
        self.wall10(0,300)
        self.wall11(100,300)
        self.wall12(300,400)
        self.wall13(200,500)
        self.wall14(200,300)
        self.wall15(200,400)

        #placement du sol
        for i in range(100,800,100):
            self.floor(400,i)
            self.floor(500,i)
            if(i!=400):
                self.floor(300,i)
            if(i!=300 and i!=100):
                self.floor(100,i)
        self.floor(200,200)
        self.floor(200,700)
        self.floor(200,600)
        self.floor(600,700)
        self.floor(600,600)

        #placemetn des etoiles 
        self.star(100,200)
        self.star(500,300)
        self.star(100,400)
        self.star(400,500)
        self.star(300,600)
        self.star(600,600)
        self.star(400,700)

        #placement des boites
        self.box1(300,200)
        self.box2(400,300)
        self.box3(400,400)
        self.box4(400,600)
        self.box5(-200,0)
        self.box6(500,600)
        self.box7(100,600)

        #placement des boites sur satr
        self.boxS1(-200,0)
        self.boxS2(-200,0)
        self.boxS3(-200,0)
        self.boxS4(-200,0)
        self.boxS5(300,600)
        self.boxS6(-200,0)
        self.boxS7(-200,0)

        #placement espace vide
        self.water(0,0,1)
        self.water(100,0,3)
        for i in range(0,500,100):
            self.water(700,i,2)
        self.water(700,400,4)

        # joueur
        self.character(200,200)
        self.characterInv(-100,0)
    

    def keyPressEvent(self,event):
        key = event.key()

        # Up
        if(key == Qt.Key_Up):
            self.__controller.moveCharacter(-1,0)
            self.__pas += 1
            self.statusBar().showMessage("Timer: "+str(self.__min)+"m "+str(self.__sec)+"s "+"  | Déplacements : " + str(self.__pas))

        #Left 
        elif(key == Qt.Key_Left):
            self.__model.setrotCharacter(-1)
            self.__controller.moveCharacter(0,-1)
            self.__pas += 1
            self.statusBar().showMessage("Timer: "+str(self.__min)+"m "+str(self.__sec)+"s "+"  | Déplacements : " + str(self.__pas))
        
        #Down
        elif(key == Qt.Key_Down):
            self.__controller.moveCharacter(1,0)
            self.__pas += 1
            self.statusBar().showMessage("Timer: "+str(self.__min)+"m "+str(self.__sec)+"s "+"  | Déplacements : " + str(self.__pas))
        
        #Right
        elif(key == Qt.Key_Right):
            self.__model.setrotCharacter(1)
            self.__controller.moveCharacter(0,1)
            self.__pas += 1
            self.statusBar().showMessage("Timer: "+str(self.__min)+"m "+str(self.__sec)+"s "+"  | Déplacements : " + str(self.__pas))
        
        elif(key == Qt.Key_Escape):
            self.close()  
        
        elif(key == Qt.Key_R):
            self.restartGame()
            
        elif(key == Qt.Key_S):
            self.__cheatCodeTmp.append('s')
        elif(key == Qt.Key_A):
            self.__cheatCodeTmp.append('a')
        elif(key == Qt.Key_U):
            self.__cheatCodeTmp.append('u')
        elif(key == Qt.Key_M):
            self.__cheatCodeTmp.append('m')
        elif(key == Qt.Key_O):
            self.__cheatCodeTmp.append('o')
        elif(key == Qt.Key_N):
            self.__cheatCodeTmp.append('n')
        elif(key == Qt.Key_F):
            self.__cheatCodeTmp.append('f')
        elif(key == Qt.Key_E):
            self.__cheatCodeTmp.append('e')
        else:
            self.__cheatCodeTmp = []
        
        if (self.__cheatCodeTmp == self.__cheatCode) and (self.__controller.getBouger() == False):
            
            self.web = saumon()
            self.web.show()
            self.__controller.autoPlaying()
            self.__cheatCodeTmp=[]

    def restartGame(self):
        self.__music.stop()
        self.__controller.restart()

    def getMusic(self):
        return self.__music

    #get label
    def getLabelC(self):
        return self.__labelC
    
    def getLabelCinv(self):
        return self.__labelCinv
    
    def getLabelB1(self):
        return self.__labelB1
    
    def getLabelB2(self):
        return self.__labelB2
    
    def getLabelB3(self):
        return self.__labelB3
    
    def getLabelB4(self):
        return self.__labelB4

    def getLabelB5(self):
        return self.__labelB5
    
    def getLabelB6(self):
        return self.__labelB6
    
    def getLabelB7(self):
        return self.__labelB7
    
    ############################
    def getLabelBS1(self):
        return self.__labelBS1
    
    def getLabelBS2(self):
        return self.__labelBS2
    
    def getLabelBS3(self):
        return self.__labelBS3
    
    def getLabelBS4(self):
        return self.__labelBS4

    def getLabelBS5(self):
        return self.__labelBS5
    
    def getLabelBS6(self):
        return self.__labelBS6
    
    def getLabelBS7(self):
        return self.__labelBS7

    #get set model
    def setModel(self,model):
        self.__model = model

    def getModel(self):
        return self.__model
    
    #get set controller
    def getController(self):
        return self.__controller

    def setController(self,controller):
        self.__controller = controller
    

