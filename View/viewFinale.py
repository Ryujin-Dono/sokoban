from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtMultimedia import QSound



class Finish(QWidget):
    def __init__(self):
        super().__init__()
        #parametre de la fenetre
        self.setWindowTitle("Sokoban")
        self.setWindowIcon(QtGui.QIcon("./View/image/hellicon.png"))
        self.setGeometry(500,50,800,900)

        self.timer=QTimer()
        self.timer.timeout.connect(self.closeTime)
        self.timer.start(100)

        self.__t=0

        self.__music = QSound("./View/sound/Win.wav")
        self.__music.play()
        
        self.background()
        self.show

    def background(self):
        self.__labelBF = QLabel(self)
        self.__labelBF.setGeometry(-550,0,1600,1800)
        self.__labelBF.setStyleSheet("background-image:url(./View/image/backgroundVictory.jpg)")

        self.__labelBB = QLabel(self)
        self.__labelBB.setGeometry(-5,400,817,111)
        self.__labelBB.setStyleSheet("background-image:url(./View/image/Victory.png)")
    
    def closeTime(self):
        
        if(self.__t == 100):
            self.close()
        
        self.__t+=1

    def keyPressEvent(self,event):
        key = event.key()

        if(key == Qt.Key_Escape):
            self.close()  
        

