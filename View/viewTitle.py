from PyQt5 import QtGui
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from View.view import *
from Model.model import *
from Controller.controller import *

class Start(QWidget):
    def __init__(self):
        super().__init__()
        #parametre de la fenetre
        self.setWindowTitle("Sokoban")
        self.setWindowIcon(QtGui.QIcon("./View/image/hellicon.png"))
        self.setGeometry(500,50,800,900)

        self.__controller = None
        self.__model = None

        self.__music = QSound("./View/sound/Menutheme.wav")
        self.__music.play()

        self.__StartButton = QPushButton("")

        self.afficheButton()  

        #placement du bouton
        layout = QVBoxLayout()
        layout.addWidget(self.__StartButton)
        self.setLayout(layout)

        self.show()

    def afficheButton(self):
        self.__labelS = QLabel(self)
        self.__labelS.setGeometry(0,0,800,900)
        self.__labelS.setStyleSheet("background-color:rgb(38,38,38)")

        #creation
        self.__StartButton.setMaximumSize(800,900)
        self.__StartButton.clicked.connect(self.clickStart) 


        #creation de l'icon
        self.__StartButton.setIcon(QIcon("./View/image/title1.png"))
        self.__StartButton.setIconSize(QSize(650,300))
        self.__StartButton.setStyleSheet("background-color:rgb(38,38,38);")   

    def keyPressEvent(self,event):
        key = event.key()
        
        if(key == Qt.Key_Escape):
            self.close()
    
    #get set model
    def setModel(self,model):
        self.__model = model

    def getModel(self):
        return self.__model
    
    #get set controller
    def getController(self):
        return self.__controller

    def setController(self,controller):
        self.__controller = controller

    def clickStart(self):
        self.__music.stop()
        self.__controller.startClick()   
        self.close()  

