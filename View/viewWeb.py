from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtMultimedia import QSound


class saumon(QWidget):
    def __init__(self):
        super().__init__()
    
        self.setWindowTitle("webcam saumon pro gamer")
        self.setWindowIcon(QIcon("./View/image/red.png"))
        self.setGeometry(60,80,537,481)
        self.background()

        self.timer=QTimer()
        self.timer.timeout.connect(self.closeTime)
        self.timer.start(100)

        self.__t=0

        self.__music = QSound("./View/sound/clavier.wav")
        self.__music.play()

        self.show()
    
    def background(self):
        self.__labelS = QLabel(self)
        self.__labelS.setGeometry(0,0,537,481)
        self.__labelS.setStyleSheet("background-image:url(./View/image/saumonGameur2.png);"
        "background-color:rgb(38,38,38)")
    
    def closeTime(self):
        
        if(self.__t == 36):
            self.close()

        if(self.__t == 36):
            self.__music.stop()
        
        self.__t+=1
