from PyQt5.QtCore import * 
from PyQt5.QtGui import * 
from PyQt5.QtWidgets import * 
from PyQt5.QtMultimedia import QSound


from View.view import *
from Model.model import *
from Controller.controller import *

class ControllerStart(QWidget):
    def __init__(self):
        super().__init__()

        self.__view = None
        self.__model = None

    def startClick(self):
        model = Move()
        view = Plateau()
        controller = Controller()

        view.setModel(model)
        view.setController(controller)

        model.setView(view)
    
        controller.setView(view)
        controller.setModel(model)

        view.show()
        self.close()

        #get set View
    def getView(self):
        return self.__view

    def setView(self,newView):
        self.__view = newView
    
    #get set Model
    def getModel(self):
        return self.__model
    
    def setModel(self,newModel):
        self.__model = newModel