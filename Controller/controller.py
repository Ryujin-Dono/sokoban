from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtMultimedia import QSound

from View.view import *
from Model.model import *
from Controller.controller import *

from View.viewFinale import *

class Controller(QWidget):
    def __init__(self):
        super().__init__()

        self.__view = None
        self.__model = None
        self.__bouger = False
    
    def moveCharacter(self,x,y):
        #attribut necessaire
        characterPos = self.__model.getPosCharacter()
        possible = self.__model.getMatriceHitBox()
        boxPos= self.__model.getPosBox()
        tab = [21,22,23,24,25,26,27]
        index = -1

        #on test si il n'y a pas de mur
        if(possible[characterPos[1]//100+x][characterPos[0]//100+y] != 1):
            #on test si ce n'est pas une box
            if(possible[characterPos[1]//100+x][characterPos[0]//100+y] not in tab):
                #on change dans la matrice
                self.__model.setMatriceHitBox(characterPos[1]//100+x,characterPos[0]//100+y,4)
                self.__model.setMatriceHitBox(characterPos[1]//100,characterPos[0]//100,3)
                #deplacement
                characterPos[1]+=x*100
                characterPos[0]+=y*100
                self.__model.setPosCharacter(characterPos)

            else:
                #deplacement
                self.__bouger = True
                posBoxPush = [characterPos[1]//100+x,characterPos[0]//100+y]
                for i in range(0,len(boxPos)):
                    if(boxPos[i][1]//100 == posBoxPush[0] and boxPos[i][0]//100 == posBoxPush[1]):
                        index = i
                    
                if(possible[posBoxPush[0]+x][posBoxPush[1]+y] != 1 and possible[posBoxPush[0]+x][posBoxPush[1]+y] not in tab):
                        
                    #on change dans la matrice
                    self.__model.setMatriceHitBox(posBoxPush[0]+x,posBoxPush[1]+y,tab[index])
                    self.__model.setMatriceHitBox(posBoxPush[0],posBoxPush[1],4)
                        
                    #deplacement
                    #box
                    boxPos[index][1]+=x*100
                    boxPos[index][0]+=y*100
                    self.__model.setPosBox(index,boxPos[index])
                    #caracter
                    characterPos[1]+=x*100
                    characterPos[0]+=y*100                    
                    self.__model.setPosCharacter(characterPos)
                else:
                    QSound.play("./View/sound/steveHurt.wav")
        else:
            QSound.play("./View/sound/steveHurt.wav")

        #update image
        if(self.__model.getRotCharacter() == 1):
            # positionne le personnage vers la droite
            labelC = self.__view.getLabelC()
            labelC.move(characterPos[0],characterPos[1])
            # positionne le personnae vers la gauche
            labelCinv = self.__view.getLabelCinv()
            labelCinv.move(-100,0)        
        else:
            # positionne le personnage vers la droite
            labelC = self.__view.getLabelC()
            labelC.move(-100,0)
            # positionne le personnae vers la gauche
            labelCinv = self.__view.getLabelCinv()
            labelCinv.move(characterPos[0],characterPos[1])

        posStar = self.__model.getPosStar()
        for i in range(0,len(posStar)):
            if(boxPos[i] == posStar[i]):
                self.__tabLabelS[i].move(boxPos[i][0],boxPos[i][1])
                self.__tabLabel[i].move(-100,0)
            else:
                self.__tabLabelS[i].move(-100,0)
                self.__tabLabel[i].move(boxPos[i][0],boxPos[i][1])
        self.win()
    
   
    def win(self):
        posStar = self.__model.getPosStar()
        boxPos= self.__model.getPosBox()
        win=0

        for i in range(0,len(posStar)):
            if(boxPos[i] == posStar[i]):
                win+=1

        if(win == 7):
            self.closeGame()
    
    def restart(self):
        model = Move()
        view = Plateau()
        controller = Controller()

        view.setModel(model)
        view.setController(controller)

        model.setView(view)
    
        controller.setView(view)
        controller.setModel(model)

        view.show()
        self.__view.close()
    
    def closeGame(self):
        music = self.__view.getMusic()
        music.stop()
        self.viewfinal = Finish()
        self.viewfinal.show()
        self.__view.close()
        
    
    #get set View
    def getView(self):
        return self.__view

    def setView(self,newView):
        self.__view = newView

        self.__labelB1 = self.__view.getLabelB1()
        self.__labelB2 = self.__view.getLabelB2()
        self.__labelB3 = self.__view.getLabelB3()
        self.__labelB4 = self.__view.getLabelB4()
        self.__labelB5 = self.__view.getLabelB5()
        self.__labelB6 = self.__view.getLabelB6()
        self.__labelB7 = self.__view.getLabelB7()

        self.__labelBS1 = self.__view.getLabelBS1()
        self.__labelBS2 = self.__view.getLabelBS2()
        self.__labelBS3 = self.__view.getLabelBS3()
        self.__labelBS4 = self.__view.getLabelBS4()
        self.__labelBS5 = self.__view.getLabelBS5()
        self.__labelBS6 = self.__view.getLabelBS6()
        self.__labelBS7 = self.__view.getLabelBS7()
        
        self.__tabLabel = [
            self.__labelB1,
            self.__labelB2,
            self.__labelB3,
            self.__labelB4,
            self.__labelB5,
            self.__labelB6,
            self.__labelB7
            ]

        self.__tabLabelS = [
            self.__labelBS1,
            self.__labelBS2,
            self.__labelBS3,
            self.__labelBS4,
            self.__labelBS5,
            self.__labelBS6,
            self.__labelBS7
            ]

    def autoPlaying(self):
        if (self.__bouger != True):
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda :self.subAutoPlaying())
            self.timer.start(100)
        

    def subAutoPlaying(self):
        if (self.__indexPath < len(self.__path)):
            self.moveCharacter(self.__path[self.__indexPath][0],self.__path[self.__indexPath][1])
            self.__bouger = False
            self.__indexPath += 1
  

    #get set Model
    def getModel(self):
        return self.__model
    
    def setModel(self,newModel):
        self.__model = newModel
        self.__path = self.__model.getAutoPlayPath()
        self.__indexPath = 0
    
    def getBouger(self):
        return self.__bouger
