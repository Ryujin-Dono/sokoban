# GROUPE

    -D'Ottavio Ugo
    -Hanquez Nicolas
    -Mastrilli Gwenael
    -Ousselin Antoine
    
## Contrôles

    -Aller à gauche     : flèche de gauche
    -Aller à droite     : flèche de droite
    -Aller en haut      : flèche du haut
    -Aller en bas       : flèche du bas
    
## Cheat code

Si vous n'avez pas encore bouger,  
taper "saumonfume" fera apparaître un saumon qui arrivera à résoudre le niveau tout seul !

## Procédure pour lancer le jeu

Tout simplement exécuter le main.py