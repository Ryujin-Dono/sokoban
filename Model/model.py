import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *


class Move(QWidget):
    def __init__(self):
        super().__init__()

        self.__view = None
        self.__controller = None

        self.__rotCharater = 1

        self.__CharacterPos = [200,200]

        self.__posBox = [[300,200],[400,300],[400,400],[400,600],[300,600],[500,600],[100,600]]

        self.__posStar=[[100,200],[500,300],[400,500],[400,700],[300,600],[600,600],[100,400]]
        
        #0 = eau
        #1 = wall 
        #2 = box 
        #3 = floor
        #4 = character
        self.__matriceHitBox=[
            [0,0,1,1,1,1,1,0],
            [1,1,1,3,3,3,1,0],
            [1,3,4,21,3,3,1,0],
            [1,1,1,3,22,3,1,0],
            [1,3,1,1,23,3,1,0],
            [1,3,1,3,3,3,1,1],
            [1,27,3,25,24,26,3,1],
            [1,3,3,3,3,3,3,1],
            [1,1,1,1,1,1,1,1]]

        self.__autoPlayPath=[
            (0,1), 
            (-1,0),
            (0,1), 
            (0,1), 
            (1,0), 
            (1,0), 
            (1,0), 
            (1,0), 
            (0,-1),
            (1,0), 
            (0,1), 
            (-1,0),
            (-1,0),
            (-1,0),
            (-1,0),
            (0,-1),
            (0,-1),
            (0,-1),
            (0,1), 
            (1,0),
            (0,1),
            (1,0),
            (0,1),
            (1,0),
            (1,0),
            (0,-1),
            (0,-1),
            (1,0),
            (0,-1),
            (0,-1),
            (-1,0),
            (-1,0),
            (1,0)]

    #get set rotCharacter
    def getRotCharacter(self):
        return self.__rotCharater
    
    def setrotCharacter(self,rot):
        self.__rotCharater = rot

    #get set posStar
    def getPosStar(self):
        return self.__posStar        

    #get set posCharacter
    def getPosCharacter(self):
        return self.__CharacterPos

    def setPosCharacter(self,newPosCharacter):
        self.__CharacterPos=newPosCharacter  
    
    #get set posBox
    def getPosBox(self):
        return self.__posBox
    
    def setPosBox(self,index,nbr):
        self.__posBox[index]=nbr

    #get set matricehitBox
    def getMatriceHitBox(self):
        return self.__matriceHitBox
    
    #avec i posX et j posY
    def setMatriceHitBox(self,i,j,nbr):
        self.__matriceHitBox[i][j]=nbr
        
    #get set View
    def getView(self):
        return self.__view

    def setView(self,newView):
        self.__view = newView
    
    #get set Controller
    def getController(self):
        return self.__controller
    
    def setController(self,newController):
        self.__controller = newController

    def getAutoPlayPath(self):
        return self.__autoPlayPath
