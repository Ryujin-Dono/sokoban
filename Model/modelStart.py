from PyQt5.QtCore import * 
from PyQt5.QtGui import * 
from PyQt5.QtWidgets import * 

class ModelStart(QWidget):
    def __init__(self):
        super().__init__()

        self.__view = None
        self.__model = None
    
    #get set View
    def getView(self):
        return self.__view

    def setView(self,newView):
        self.__view = newView
    
    #get set Model
    def getModel(self):
        return self.__model
    
    def setModel(self,newModel):
        self.__model = newModel