from View.viewTitle import Start
import sys
from PyQt5.QtCore import * 
from PyQt5.QtGui import * 
from PyQt5.QtWidgets import * 
from PyQt5.QtMultimedia import QSound

from View.viewTitle import *
from Model.modelStart import *
from Controller.controllerStart import *

from View.view import *
from Model.model import *
from Controller.controller import *

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        viewStart= Start()
        controllerStart = ControllerStart()
        modelStart = ModelStart()

        viewStart.setController(controllerStart)
        viewStart.setModel(modelStart)
    

        modelStart.setView(viewStart)
    
        controllerStart.setView(viewStart)
        controllerStart.setModel(modelStart)

        self.menuBarGame()
        

        viewStart.show()

    def menuBarGame(self):
        self.bar = self.menuBar()
        self.setMenuBar(self.bar)
        self.Jeu = self.bar.addMenu("Jeu")

        self.quitGame = QAction("Quit",self)
        self.quitGame.setShortcut("Ctrl+Q")
        self.Jeu.addAction(self.quitGame)
        self.quitGame.triggered.connect(self.boutonQ)

        self.restartGame = QAction("Restart",self)
        self.restartGame.setShortcut("Ctrl+R")
        self.Jeu.addAction(self.restartGame)
        self.restartGame.triggered.connect(self.boutonR)

    def boutonR(self):
        model = Move()
        view = Plateau()
        controller = Controller()

        view.setModel(model)
        view.setController(controller)

        model.setView(view)
    
        controller.setView(view)
        controller.setModel(model)

        view.show()
        self.__view.close()

    def boutonQ(self):
        sys.exit(app.exec_())

    



if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())

